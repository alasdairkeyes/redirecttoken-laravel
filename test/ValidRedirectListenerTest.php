<?php

namespace Test;

use RedirectToken\Laravel\Listeners\ValidRedirect;
use RedirectToken\Laravel\Event\ValidRedirectRequest;
use Illuminate\Http\Request;

/**
 * Class ValidRedirectListenerTest
 * @package Tests
 */
class ValidRedirectListenerTest extends TestCase
{
    public function testInstantiation()
    {
        $listener = new ValidRedirect();
        $this->assertInstanceOf(
            'RedirectToken\Laravel\Listeners\ValidRedirect',
            $listener
        );
    }

    public function testHandle()
    {
        // Get Requirements and Build up Mock Event
        $request = new Request();
        $uriString = self::TEST_REDIRECT_DESTINATION;
        $token = self::VALID_URL_TOKEN;

        $mockEvent = $this->getMockBuilder(ValidRedirectRequest::class)
            ->setConstructorArgs([$uriString, $token, $request])
            ->getMock();

        $listener = new ValidRedirect();
        $this->assertNull($listener->handle($mockEvent));
    }
}
