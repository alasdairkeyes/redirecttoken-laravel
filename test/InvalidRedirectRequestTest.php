<?php

namespace Test;

use RedirectToken\Laravel\Event\InvalidRedirectRequest;
use Illuminate\Http\Request;

/**
 * Class InvalidRedirectRequestTest
 * @package Tests
 */
class InvalidRedirectRequestTest extends TestCase
{
    public function testInstantiate()
    {
        $invalidRedirectEvent = $this->instantiateEvent();

        $this->assertInstanceOf(
            'RedirectToken\Laravel\Event\InvalidRedirectRequest',
            $invalidRedirectEvent
        );
    }

    public function testAttributes()
    {
        $invalidRedirectEvent = $this->instantiateEvent();
        $this->assertEquals(self::TEST_REDIRECT_DESTINATION, $invalidRedirectEvent->uriString);
        $this->assertEquals(self::VALID_URL_TOKEN, $invalidRedirectEvent->token);
        $this->assertInstanceOf('Illuminate\Http\Request', $invalidRedirectEvent->request);
    }

    /**
     * Instantiate InvalidRedirectRequest Event Object
     *
     * @return RedirectToken\Laravel\Event\InvalidRedirectRequest
     */
    protected function instantiateEvent()
    {
        $request = new Request();
        return new InvalidRedirectRequest(
            self::TEST_REDIRECT_DESTINATION,
            self::VALID_URL_TOKEN,
            $request
        );
    }
}
