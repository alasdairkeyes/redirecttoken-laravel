<?php

namespace Test;

use Illuminate\Http\Request;

/**
 * Class ControllerTest
 * @package Tests
 */
class ControllerTest extends TestCase
{
    public function testGetRequestValid()
    {
        $request = $this->get(
            self::VALID_REDIRECTION_URI
        );
        $request->assertRedirect(self::TEST_REDIRECT_DESTINATION);
    }

    public function testGetRequestInvalidToken()
    {
        $request = $this->get(
            '/fwd?uri=https%3A%2F%2Fwww.gitlab.com%2Falasdairkeyes%2Fredirecttoken&token=junkToken'
        );
        $request->assertStatus(400);
    }

    public function testGetRequestInvalidNoTokenParam()
    {
        $request = $this->get(
            '/fwd?uri=https%3A%2F%2Fwww.gitlab.com%2Falasdairkeyes%2Fredirecttoken'
        );
        $request->assertStatus(400);
    }

    public function testGetRequestInvalidNoUriParam()
    {
        $request = $this->get(
            '/fwd?token=junkToken'
        );
        $request->assertStatus(400);
    }

    public function testGetRequestInvalidNoParams()
    {
        $request = $this->get(
            '/fwd'
        );
        $request->assertStatus(400);
    }
}
