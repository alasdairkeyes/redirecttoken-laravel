<?php

namespace Test;

use RedirectToken\Laravel\Event\ValidRedirectRequest;
use Illuminate\Http\Request;

/**
 * Class ValidRedirectRequestTest
 * @package Tests
 */
class ValidRedirectRequestTest extends TestCase
{
    public function testInstantiate()
    {
        $validRedirectEvent = $this->instantiateEvent();

        $this->assertInstanceOf(
            'RedirectToken\Laravel\Event\ValidRedirectRequest',
            $validRedirectEvent
        );
    }

    public function testAttributes()
    {
        $validRedirectEvent = $this->instantiateEvent();
        $this->assertEquals(self::TEST_REDIRECT_DESTINATION, $validRedirectEvent->uriString);
        $this->assertEquals(self::VALID_URL_TOKEN, $validRedirectEvent->token);
        $this->assertInstanceOf('Illuminate\Http\Request', $validRedirectEvent->request);
    }

    /**
     * Instantiate InvalidRedirectRequest Event Object
     *
     * @return RedirectToken\Laravel\Event\InvalidRedirectRequest
     */
    protected function instantiateEvent()
    {
        $request = new Request();
        return new ValidRedirectRequest(
            self::TEST_REDIRECT_DESTINATION,
            self::VALID_URL_TOKEN,
            $request
        );
    }
}
