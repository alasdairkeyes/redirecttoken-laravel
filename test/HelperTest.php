<?php

namespace Test;

use RedirectToken\Laravel\Support\RedirectTokenHelper;

/**
 * Class HelperTest
 * @package Tests
 */
class HelperTest extends TestCase
{
    public function testRedirectTokenGenerateHelper()
    {
        $actual = RedirectTokenHelper::redirectTokenGenerate(self::TEST_REDIRECT_DESTINATION);
        $this->assertSame(self::VALID_URL_TOKEN, $actual);
    }

    public function testRedirectTokenValidateHelperValidToken()
    {
        $actual = RedirectTokenHelper::redirectTokenValidate(self::TEST_REDIRECT_DESTINATION, self::VALID_URL_TOKEN);
        $this->assertTrue($actual);
    }

    public function testRedirectTokenValidateHelperInvalidToken()
    {
        $actual = RedirectTokenHelper::redirectTokenValidate(self::TEST_REDIRECT_DESTINATION, self::INVALID_URL_TOKEN);
        $this->assertFalse($actual);
    }

    public function testRedirectTokenGenerateUri()
    {
        $actual = RedirectTokenHelper::redirectTokenGenerateUri(self::TEST_REDIRECT_DESTINATION);
        $this->assertSame(
            self::VALID_REDIRECTION_URI,
            $actual
        );
    }
}
