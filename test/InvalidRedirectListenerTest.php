<?php

namespace Test;

use RedirectToken\Laravel\Listeners\InvalidRedirect;
use RedirectToken\Laravel\Event\InvalidRedirectRequest;
use Illuminate\Http\Request;

/**
 * Class InvalidRedirectListenerTest
 * @package Tests
 */
class InvalidRedirectListenerTest extends TestCase
{
    public function testInstantiation()
    {
        $listener = new InvalidRedirect();
        $this->assertInstanceOf(
            'RedirectToken\Laravel\Listeners\InvalidRedirect',
            $listener
        );
    }

    public function testHandle()
    {
        // Get Requirements and Build up Mock Event
        $request = new Request();
        $uriString = self::TEST_REDIRECT_DESTINATION;
        $token = self::VALID_URL_TOKEN;

        $mockEvent = $this->getMockBuilder(InvalidRedirectRequest::class)
            ->setConstructorArgs([$uriString, $token, $request])
            ->getMock();

        $listener = new InvalidRedirect();
        $this->assertNull($listener->handle($mockEvent));
    }
}
