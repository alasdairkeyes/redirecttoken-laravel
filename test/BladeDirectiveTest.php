<?php

namespace Test;

/**
 * Class BladeDirectiveTest
 * @package Test
 */
class BladeDirectiveTest extends TestCase
{
    public function testDirective()
    {
        $compiler = $this->getCompiler();
        $actual = $compiler->compileString('@redirectUrl("https:://www.fsf.org/")');
        $expected = sprintf(
            "<?php echo RedirectTokenHelper::redirectTokenGenerateUri(%s, %s, %s, %s); ?>",
            '"https:://www.fsf.org/"',
            "config('redirecttoken.path')",
            "config('redirecttoken.uri_query_key')",
            "config('redirecttoken.token_query_key')"
        );
        $this->assertSame($expected, $actual);
    }
}
