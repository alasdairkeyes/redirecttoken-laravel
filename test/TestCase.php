<?php

namespace Test;

use Illuminate\View\Compilers\BladeCompiler;
use RedirectToken\Laravel\Providers\RedirectTokenServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

/**
 * Class TestCase
 * @package Tests
 */
abstract class TestCase extends OrchestraTestCase
{
    // Config settings to use for testing
    const TEST_SECRET = '0123456789abcdef';
    const TEST_HASH_ALGO = 'sha256';
    const TEST_TOKEN_PATH = '/fwd';
    const TEST_URI_QUERY_KEY = 'uri';
    const TEST_TOKEN_QUERY_KEY = 'token';

    // URI to forward to for testing
    const TEST_REDIRECT_DESTINATION = 'https://www.gitlab.com/alasdairkeyes/redirecttoken';

    // Valid token for the above Settings/URI
    const VALID_URL_TOKEN = '428c9aaccace42d47e7473035b82e9e6323efb0750a4370511a39cabf8251692';

    // Invalid token
    const INVALID_URL_TOKEN = 'junkToFailTheTest';

    // Valid Redirection URI generated for all above settings
    const VALID_REDIRECTION_URI = '/fwd?uri=https%3A%2F%2Fwww.gitlab.com%2Falasdairkeyes%2Fredirecttoken&token=428c9aaccace42d47e7473035b82e9e6323efb0750a4370511a39cabf8251692';

    /**
     * @var BladeCompiler
     */
    protected $compiler;

    /**
     * @return BladeCompiler
     */
    protected function getCompiler()
    {
        if (!$this->compiler) {
            $this->compiler = $this->app->make(BladeCompiler::class);
        }
        return $this->compiler;
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        // Set config here as the standard Orchestra bootstrapping using
        // getEnvironmentSetup() doesn't load config in time before
        // calling register() on the Service Provider
        $this->setConfig();
        return [
            RedirectTokenServiceProvider::class
        ];
    }

    /**
     * Set config for plugin
     * @return void
     */
    protected function setConfig()
    {
        $app = app();
        $app['config']->set('redirecttoken.secret', self::TEST_SECRET);
        $app['config']->set('redirecttoken.hash_algo', self::TEST_HASH_ALGO);
        $app['config']->set('redirecttoken.path', self::TEST_TOKEN_PATH);
        $app['config']->set('redirecttoken.uri_query_key', self::TEST_URI_QUERY_KEY);
        $app['config']->set('redirecttoken.token_query_key', self::TEST_TOKEN_QUERY_KEY);
    }
}
