<?php

namespace RedirectToken\Laravel\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RedirectToken\Laravel\Support\RedirectTokenHelper;
use RedirectToken\Laravel\Enum;
use RedirectToken\Laravel\Event\ValidRedirectRequest;
use RedirectToken\Laravel\Event\InvalidRedirectRequest;

/**
 * Class RedirectTokenController
 * @package App\Controllers
 */
class RedirectTokenController extends Controller
{
    /**
     * Take a requested URL and hash and forward if successful
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get input params
        $uriString = $request->input(config('redirecttoken.uri_query_key'));
        $token = $request->input(config('redirecttoken.token_query_key'));

        if ($uriString === null or $token === null) {
            event(new InvalidRedirectRequest($uriString, $token, $request));
            abort(400, 'Invalid redirect');
        }
        
        // Validate and redirect
        if (RedirectTokenHelper::redirectTokenValidate($uriString, $token)) {
            event(new ValidRedirectRequest($uriString, $token, $request));
            return redirect()->away($uriString);
        }

        // Or fail
        event(new InvalidRedirectRequest($uriString, $token, $request));
        abort(400, 'Invalid redirect');
    }
}
