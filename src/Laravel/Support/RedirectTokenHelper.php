<?php

namespace RedirectToken\Laravel\Support;

use GuzzleHttp\Psr7\Uri;

/**
 * Helper functions for RedirectToken in Laravel
 */

class RedirectTokenHelper
{
    /**
     * Helper function to generate a token for a given URI
     *
     * @param string $urlToForwardTo
     * @return string
     */
    public static function redirectTokenGenerate($urlToForwardTo)
    {
        /**
         * @var RedirectToken\Generator $generator
         */
        $generator = app('redirectTokenGenerator');
        $uri = new Uri($urlToForwardTo);
        return $generator->generateToken($uri);
    }

    /**
     * Helper function to validate given URI/Token
     *
     * @param string $urlToForwardTo
     * @param string $token
     * @return bool
     */
    public static function redirectTokenValidate($urlToForwardTo, $token)
    {
        /**
         * @var RedirectToken\Validator $validator
         */
        $validator = app('redirectTokenValidator');
        $uri = new Uri($urlToForwardTo);

        return $validator->validateUriToken($uri, $token);
    }

    /**
     * Generate a the forward path for a URI
     *
     * @param string $urlToForwardTo
     * @return string
     */
    public static function redirectTokenGenerateUri($urlToForwardTo)
    {
        /**
         * @var string $token
         */
        $token = self::redirectTokenGenerate($urlToForwardTo);
        $fwdPath = config('redirecttoken.path');
        $uriQueryKey = config('redirecttoken.uri_query_key');
        $tokenQueryKey = config('redirecttoken.token_query_key');

        return $fwdPath . '?' . http_build_query([ $uriQueryKey => $urlToForwardTo, $tokenQueryKey => $token], '', '&');
    }
}
