<?php

namespace RedirectToken\Laravel\Event;

use Illuminate\Http\Request;

class InvalidRedirectRequest
{
    /** @var string $uriString */
    public $uriString;

    /** @var string $token */
    public $token;

    /** @var Request $request */
    public $request;

    /**
     * Take a requested URL and hash and forward if successful
     *
     * @param string $uriString
     * @param string $token
     * @param \Illuminate\Http\Request $request
     */
    public function __construct($uriString, $token, Request $request)
    {
        $this->uriString = $uriString;
        $this->token = $token;
        $this->request = $request;
    }
}
