<?php

namespace RedirectToken\Laravel\Providers;

use Illuminate\Support\ServiceProvider;
use RedirectToken\Laravel\Console\Commands\GenerateRedirectTokenCommand;
use RedirectToken\Generator;
use RedirectToken\Validator;
use RedirectToken\Laravel\Enum;

/**
 * Class RedirectTokenServiceProvider
 * @package App\Providers
 */
class RedirectTokenServiceProvider extends ServiceProvider
{
    /**
     * Boot the RedirectToken Service
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../../routes/routes.php');

        // Define the config publisher
        $this->publishes([
            __DIR__ . '/../../../config/redirecttoken.php' => config_path('redirecttoken.php'),
        ], 'config');

        // Define the event listener publisher
        $this->publishes([
            __DIR__ . '/../../../src/Laravel/Listeners/ValidRedirect.php'
                => app_path('Listeners/ValidRedirect.php'),
            __DIR__ . '/../../../src/Laravel/Listeners/InvalidRedirect.php'
                => app_path('Listeners/InvalidRedirect.php'),
        ], 'listeners');

        // Define the redirectUrl() directive
        $this->compiler()->directive('redirectUrl', function ($urlToForwardTo) {
            return '<?php echo RedirectTokenHelper::redirectTokenGenerateUri('
                . $urlToForwardTo . ', '
                . "config('redirecttoken.path'), "
                . "config('redirecttoken.uri_query_key'), "
                . "config('redirecttoken.token_query_key')"
                . "); ?>";
        });

        // Define the Commands to publish
        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateRedirectTokenCommand::class,
            ]);
        }
    }

    /**
     * Get the BladeCompiler from the Template
     *
     * @return BladeCompiler
     */
    protected function compiler()
    {
        return app('view')->getEngineResolver()->resolve('blade')->getCompiler();
    }

    /**
     * Add RedirectToken objects to service container
     *
     * @return void
     */
    public function register()
    {
        // Load config data and store
        $secret = config('redirecttoken.secret');
        $hashAlgo = config('redirecttoken.hash_algo');

        $this->app->singleton(Enum::GENERATOR_KEY, function () use ($secret, $hashAlgo) {
            return new Generator($secret, $hashAlgo);
        });

        $this->app->singleton(Enum::VALIDATOR_KEY, function () use ($secret, $hashAlgo) {
            return new Validator($secret, $hashAlgo);
        });
    }
}
