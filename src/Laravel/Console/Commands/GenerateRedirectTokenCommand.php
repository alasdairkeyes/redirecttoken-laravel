<?php

namespace RedirectToken\Laravel\Console\Commands;

use Illuminate\Console\Command;
use RedirectToken\Laravel\Support\RedirectTokenHelper;

class GenerateRedirectTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redirecttoken:generate {forwardUrl}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the redirect token for a given URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = $this->argument('forwardUrl');
        printf(
            "URL: %s%sHash: %s%s\nRedirect URL: %s%s",
            $url,
            PHP_EOL,
            RedirectTokenHelper::redirectTokenGenerate($url),
            PHP_EOL,
            RedirectTokenHelper::redirectTokenGenerateUri($url),
            PHP_EOL
        );
    }
}

