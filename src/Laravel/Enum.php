<?php

namespace RedirectToken\Laravel;

/**
 * Enum Class
 *
 * Enum class provides constants for use in the package
 */
class Enum
{
    /**
     * Service Container Key for validator
     *
     * @const string
     */
    const GENERATOR_KEY = 'redirectTokenGenerator';

    /**
     * Service Container Key for validator
     *
     * @const string
     */
    const VALIDATOR_KEY = 'redirectTokenValidator';
}
