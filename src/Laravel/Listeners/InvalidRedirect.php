<?php

namespace RedirectToken\Laravel\Listeners;

use RedirectToken\Laravel\Event\InvalidRedirectRequest;
use Illuminate\Support\Facades\Log;

/**
 * Class InvalidRedirect
 * @package App\Listeners
 */
class InvalidRedirect
{
    /**
     * Handle the event.
     *
     * @param  \RedirectToken\Laravel\Event\InvalidRedirectRequest  $event
     * @return void
     */
    public function handle(InvalidRedirectRequest $event)
    {
        Log::info(
            "Got invalid redirect to "
            . $event->uriString
            . ' with token ' . $event->token
            . ' via ' . $event->request->path()
        );
    }
}
