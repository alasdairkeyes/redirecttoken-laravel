<?php

namespace RedirectToken\Laravel\Listeners;

use RedirectToken\Laravel\Event\ValidRedirectRequest;
use Illuminate\Support\Facades\Log;

/**
 * Class ValidRedirect
 * @package App\Listeners
 */
class ValidRedirect
{
    /**
     * Handle the event.
     *
     * @param  \RedirectToken\Laravel\Event\ValidRedirectRequest  $event
     * @return void
     */
    public function handle(ValidRedirectRequest $event)
    {
        Log::info(
            "Got valid redirect to "
            . $event->uriString
            . ' with token ' . $event->token
            . ' via ' . $event->request->path()
        );
    }
}
