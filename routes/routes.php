<?php

/**
 * Define routes for RedirectToken
 *
 * Route is specific to the web routes only
 */

Route::group(
    [
        'middleware' => ['web'],
        'namespace' => '\RedirectToken\Laravel\Http\Controllers'
    ],
    function () {
        Route::get(config('redirecttoken.path'), 'RedirectTokenController@index');
    }
);
