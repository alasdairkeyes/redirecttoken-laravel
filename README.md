[![pipeline status](https://gitlab.com/alasdairkeyes/redirecttoken-laravel/badges/master/pipeline.svg)](https://gitlab.com/alasdairkeyes/redirecttoken-laravel/commits/master)
[![coverage report](https://gitlab.com/alasdairkeyes/redirecttoken-laravel/badges/master/coverage.svg)](https://gitlab.com/alasdairkeyes/redirecttoken-laravel/commits/master)
[![Latest Version on Packagist](https://img.shields.io/packagist/v/alasdairkeyes/redirecttoken-laravel.svg)](https://packagist.org/packages/alasdairkeyes/redirecttoken-laravel)
[![Total Downloads](https://img.shields.io/packagist/dt/alasdairkeyes/redirecttoken-laravel.svg)](https://packagist.org/packages/alasdairkeyes/redirecttoken-laravel)

# redirecttoken-laravel

Laravel integration for the redirecttoken PHP package

## Description

This package implements the redirecttoken (https://gitlab.com/alasdairkeyes/redirecttoken) for the Laravel framework.

This redirects website visitors to the URI with a 302 redirect. If an invalid request is received a 400 response is returned.

## Installation

* Add the package to composer with the following
```
composer require alasdairkeyes/redirecttoken-laravel
```

## Configuration

* Publish the config

`php artisan vendor:publish --provider="RedirectToken\Laravel\Providers\RedirectTokenServiceProvider" --tag=config`

* Update your environment with a secret at least 10 characters long. This is a minimum length, the longer the string, the better.

`REDIRECT_TOKEN_SECRET=MyIncrediblySecureSecret`

* If you're using config caching, refresh your cache

`php artisan config:cache`

**If you're using Laravel >=5.5 you can stop here. The rest of the setup will auto-load.**

For Lumen and Laravel <5.5 do the following...

* Add the Service Provider

Add the Class to your `app/config.php` config file

```
'providers' => [
    // Other providers above...
    RedirectToken\Laravel\Providers\RedirectTokenServiceProvider::class,
    // Other providers below...
],
```

* Add the Aliases

Add the Alias to your `app/config.php` config file

```
'aliases' => [
        // Other aliases above...
        'RedirectTokenHelper' => RedirectToken\Laravel\Support\RedirectTokenHelper::class,
        // Other aliases below...
    ],
```


## Example

### In Templates

In your template just add the following directive into your `href` value

`<a href="@redirectUrl('https://www.somewebsite.com')">Redirect To Some Website</a>`

Or you can also use a variable

`<a href="@redirectUrl($websiteToRedirectTo)">Redirect To Some Website</a>`

### On the Command Line

You can also generate the token and redirect URL from the command line using artisan

```
$ ./artisan redirecttoken:generate "https://gitlab.com/alasdairkeyes/redirecttoken-laravel"
URL: https://gitlab.com/alasdairkeyes/redirecttoken-laravel
Hash: 6898e1280454fc921f3af130e34e02cf39c5731fc52782a207811c9b5d9f5804

Redirect URL: /fwd?uri=https%3A%2F%2Fgitlab.com%2Falasdairkeyes%2Fredirecttoken-laravel&token=6898e1280454fc921f3af130e34e02cf39c5731fc52782a207811c9b5d9f5804
```

## Extending functionality

The following settings can be updated by updating your `.env` file

### Forwarding Path

By default the forwarding path in your application is `/fwd`. This can be changed by using the following

`REDIRECT_TOKEN_PATH=/forwarder`

### Hashing Algorithm

This uses the `sha256` hashing algorithm to generate the token. This can be changed with

`REDIRECT_TOKEN_HASH_ALGO=sha512`

### Query keys

The default query string generated uses the query parameters `uri` and `token`

This can be changed with

`REDIRECT_TOKEN_URI_QUERY_KEY=redirectto`

`REDIRECT_TOKEN_TOKEN_QUERY_KEY=checksum`

### Custom error page

You can customize the page returned when an invalid redirect request is received by creating a `resources/views/errors/400.blade.php`

### Events

The following events are used

* When a valid redirect is received - `RedirectToken\Laravel\Event\ValidRedirectRequest`
* When an invalid redirect is received `RedirectToken\Laravel\Event\InvalidRedirectRequest`

You can listen and handle these events by adding the following to the `$listen` variable in `app/Providers/EventServiceProvider.php`

```
protected $listen = [
	'RedirectToken\Laravel\Event\ValidRedirectRequest' => [
		'App\Listeners\ValidRedirect',
	]
];
```

Some example listeners are included and can be added to your app with the following command

`php artisan vendor:publish --provider="RedirectToken\Laravel\Providers\RedirectTokenServiceProvider" --tag=listeners`

## Site

https://gitlab.com/alasdairkeyes/redirecttoken-laravel


## Author

* Alasdair Keyes - https://akeyes.co.uk/


## License

Released under GPL V3 - See included license file.
