#!/bin/bash

# Move to route of the repo
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd ${DIR}/../

vendor/bin/phpmd src/ text cleancode,codesize,controversial,design,unusedcode,naming
