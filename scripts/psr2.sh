#!/bin/bash

# Move to route of the repo
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd ${DIR}/../

vendor/bin/phpcs --standard=psr2 src/
