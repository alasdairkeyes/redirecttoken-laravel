<?php

return [

 /*
    |--------------------------------------------------------------------------
    | Redirect Token Secret
    |--------------------------------------------------------------------------
    |
    | This is the key used for creating the hash for URI forwarding validation
    |
    */

    'secret' => env('REDIRECT_TOKEN_SECRET'),

    /*
    |--------------------------------------------------------------------------
    | Redirect Token Hashing Algorithm
    |--------------------------------------------------------------------------
    |
    | This is the key used for creating the hash for URl forwarding validation
    |
    */

    'hash_algo' => env('REDIRECT_TOKEN_HASH_ALGO', 'sha256'),

    /*
    |--------------------------------------------------------------------------
    | Redirect Token Path
    |--------------------------------------------------------------------------
    |
    | This is the url that is used to process the redirection
    |
    */

    'path' => env('REDIRECT_TOKEN_PATH', '/fwd'),

    /*
    |--------------------------------------------------------------------------
    | Redirect Token URI Query Key
    |--------------------------------------------------------------------------
    |
    | This is the GET query key used to specify the URI to redirect to
    |
    */

    'uri_query_key' => env('REDIRECT_TOKEN_URI_QUERY_KEY', 'uri'),

    /*
    |--------------------------------------------------------------------------
    | Redirect Token Token Query Key
    |--------------------------------------------------------------------------
    |
    | This is the GET query key used to specify the URI to redirect to
    |
    */

    'token_query_key' => env('REDIRECT_TOKEN_TOKEN_QUERY_KEY', 'token'),

];
